﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControladorInputs : MonoBehaviour {

    Object[] nuevoObjeto;
    public static string[] nombres;
    GameObject[] entidadesMundo = new GameObject[200];
    public Dropdown dropEntidades, dropTipos, dropMundo;

    void Start()
    {
        nombres = new string[100];
        cargaEntidades();
    }

    public void cargaEntidades()
    {
        dropEntidades.ClearOptions();
        nuevoObjeto = Resources.LoadAll("Prefabs/Mundos/" + NombreCarpetaMundo() + "/" + NombreCarpetaTipo() + "/");
           
        for (int i = 0; i < nuevoObjeto.Length; i++)
        {
            entidadesMundo[i] = (GameObject)Instantiate(nuevoObjeto[i]);
            string[] nombre = entidadesMundo[i].name.Split('(');
            dropEntidades.options.Add(new Dropdown.OptionData() { text = nombre[0] });
            nombres[i] = nombre[0];
        }

        dropEntidades.value = 1;              
    }

    public string NombreCarpetaMundo()
    {
        if (dropMundo.value == 0) return "Ciudad";
        if (dropMundo.value == 1) return "Desierto";
        if (dropMundo.value == 2) return "jungla";
        return "Ciudad";

    }

    public string NombreCarpetaTipo()
    {
        if (dropTipos.value == 0) return "Atrezzo";
        if (dropTipos.value == 1) return "Entidades";
        return "Atrezzo";
    }
	
}
