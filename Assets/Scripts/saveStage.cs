﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEditor;

public class saveStage : MonoBehaviour {

    public InputField nombreFase;
    public Dropdown dropMundo;
    public Text txtResultado;
    public GameObject fase;

    public void Guarda()
    {

        if (nombreFase.text == "" || nombreFase.text == " ")
        {
            txtResultado.text = "Introduce un nombre correcto";
        }
        else
        {
            PrefabUtility.CreatePrefab("Assets/Resources/Prefabs/Fases/" + NombreCarpetaMundo() + "/" + nombreFase.text + ".prefab", fase);
            txtResultado.text = "Guardado con éxito";
        }

    }

    public string NombreCarpetaMundo()
    {
        if (dropMundo.value == 0) return "Ciudad";
        if (dropMundo.value == 1) return "Desierto";
        if (dropMundo.value == 2) return "jungla";
        return "Ciudad";

    }
}
