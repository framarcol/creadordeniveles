﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEditor;

public class crearNuevasEntidades : MonoBehaviour {

    public Dropdown dropMundo, dropTipos, dropColor;
    public InputField nombrePrefab;
    public InputField ancho, alto;
    public GameObject entidadAGuardar;
    public Text txtResultado;
    Vector3 posicionNivel;

    void Start()
    {
        posicionNivel = new Vector3(-50, 9, 0);
    }

    public void Guarda()
    {

        Text txt = entidadAGuardar.GetComponentInChildren<Text>();

        RectTransform rect = entidadAGuardar.GetComponent<RectTransform>();

        BoxCollider colli = entidadAGuardar.GetComponent<BoxCollider>();

        Image imag = entidadAGuardar.GetComponent<Image>();

        float anchito =  int.Parse(ancho.text);
        float altito = int.Parse(alto.text);
        
        rect.sizeDelta = new Vector2(anchito, altito);
        colli.size = new Vector3(anchito +10, altito+10, colli.size.z);
        imag.color = ColorEntidad();

        txt.text = nombrePrefab.text;

        if (nombrePrefab.text == "" || nombrePrefab.text == " ")
        {
            txtResultado.text = "Introduce un nombre correcto";
        }
        else
        {
            PrefabUtility.CreatePrefab("Assets/Resources/Prefabs/Mundos/" + NombreCarpetaMundo() + "/"  + NombreCarpetaTipo() + "/" + nombrePrefab.text + ".prefab", entidadAGuardar);
            txtResultado.text = "Guardado con éxito";
        }

    }

    public Color ColorEntidad()
    {
        if (dropColor.value == 0) return Color.red;
        if (dropColor.value == 1) return Color.yellow;
        if (dropColor.value == 2) return Color.gray;
        if (dropColor.value == 3) return Color.green;
        if (dropColor.value == 4) return Color.black;
        return Color.black;
    }

    public string NombreCarpetaMundo()
    {
        if (dropMundo.value == 0) return "Ciudad";
        if (dropMundo.value == 1) return "Desierto";
        if (dropMundo.value == 2) return "jungla";
        return "Ciudad";

    }

    public string NombreCarpetaTipo()
    {
        if (dropTipos.value == 0) return "Atrezzo";
        if (dropTipos.value == 1) return "Entidades";
        return "Atrezzo";
    }


}
