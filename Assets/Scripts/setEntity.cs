﻿using UnityEngine;
using System.Collections;

public class setEntity : MonoBehaviour {


    public static Collider collEntity;
    public static GameObject objetoEntity;

    void Update()
    {
        if (makeEntity.hayEntidad)
        {
            if (makeEntity.esFase == false)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Input.GetMouseButton(0)) if (collEntity.Raycast(ray, out hit, 400.0F))
                    {
                        Vector3 miVector = ray.GetPoint(400.0F);

                        miVector.z = objetoEntity.transform.position.z;

                        objetoEntity.transform.position = miVector;
                    }
            }
        }

        if (Input.GetKeyDown("delete"))
            BorrarEntidad();
    }

    void BorrarEntidad()
    {
        Destroy(objetoEntity);
    }

    




}
