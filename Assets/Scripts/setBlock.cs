﻿using UnityEngine;
using System.Collections;

public class setBlock : MonoBehaviour {


    public static Collider collBlock;
    public static GameObject objetoBlock;
    float hit2;
    int contador = 0;
    void Start()
    {
        hit2 = 100.0F;
    }

    void Update()
    {
        if (makeEntity.esFase == true)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Input.GetMouseButton(0)) if (collBlock.Raycast(ray, out hit, hit2))
                    {
                        Vector3 miVector = ray.GetPoint(hit2);
                        miVector.z = objetoBlock.transform.position.z;
                        objetoBlock.transform.position = miVector;
                    }
        }

        if (Input.GetKeyDown("delete"))
            BorrarEntidad();
    }

    public void Girar()
    {
        switch (contador)
        {
            case 0:
            objetoBlock.transform.Rotate(new Vector3(0, 0, -60.5f));
            contador++;
            break;
            case 1:
            objetoBlock.transform.Rotate(new Vector3(0, 0, -29.5f));
            contador++;
            break;
            case 2:
            objetoBlock.transform.Rotate(new Vector3(0, 0, -30.1f));
            contador++;
            break;
            case 3:
            objetoBlock.transform.Rotate(new Vector3(0, 0, -59.95f));
            contador++;
            break;
            case 4:
            objetoBlock.transform.Rotate(new Vector3(0, 0, -60));
            contador++;
            break;
            case 5:
            objetoBlock.transform.Rotate(new Vector3(0, 0, 240.05f));
            contador = 0;
            break;


        }
        
    }

    void BorrarEntidad()
    {
        Destroy(objetoBlock);
    }


}
