﻿using UnityEngine;
using System.Collections;

public class MenuController : MonoBehaviour {

    public Canvas[] canvasMenu;
    public Canvas menuPrinciopal;
    public Canvas btnEspecial;
    public GameObject objetoEscena;

    public void Start()
    {
        for (int i = 0; i < canvasMenu.Length; i++) canvasMenu[i].gameObject.SetActive(false);
        menuPrinciopal.gameObject.SetActive(true);
        btnEspecial.gameObject.SetActive(false);
        makeEntity.esStage = false;
        makeEntity.esCrearBloque = false;
        Camera.main.transform.position = new Vector3(0, 0, 0);

    }

    public void CambiaCanvas(int numCanvas)
    {
        canvasMenu[numCanvas].gameObject.SetActive(true);
        menuPrinciopal.gameObject.SetActive(false);
        btnEspecial.gameObject.SetActive(true);
        makeEntity.esFase = true;
        if (numCanvas == 0) makeEntity.esCrearBloque = true;
    }

    public void CambiaCamara(bool estado)
    {
        Camera.main.orthographic = estado;
        if (!estado) Camera.main.fieldOfView = 160;
    }

    public void btnVolver()
    {
        Start();
    }

    public void MuestraFases()
    {
        canvasMenu[5].gameObject.SetActive(true);
    }

    public void MuestraFaseEditar()
    {
        canvasMenu[6].gameObject.SetActive(true);
    }

    public void MuestraNivelPrefabreados()
    {
        canvasMenu[7].gameObject.SetActive(true);
    }
}
