﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Tabulador : MonoBehaviour {

    public InputField m1, m2, m3;

    void Update () {

        if (Input.GetKeyDown("tab"))
        {
            if (m1.isFocused)
            {
                m2.ActivateInputField();
            }
            else if (m2.isFocused)
            {
                m3.ActivateInputField();
            }
            else if (m3.isFocused)
            {
                m1.ActivateInputField();
            }
        }
            

    }

}
