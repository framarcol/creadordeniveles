﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadBlocks : MonoBehaviour {


    public Object[] nuevoObjeto;
    GameObject[] entidadesMundo = new GameObject[200];
    public Dropdown dropBloques, dropMundo;
    GameObject entidad;
    string[] bloque;
    public GameObject entidadPadre;
    float nuevaPosicion;
    public Text cuentaBloques;
    int contadorBloques;


    public void Start()
    {
        bloque = new string[1000];
        nuevaPosicion = entidadPadre.transform.position.z;
    }

    public void cargaEntidades()
    {
        dropBloques.ClearOptions();
        nuevoObjeto = Resources.LoadAll("Prefabs/Niveles/" + NombreCarpetaMundo() + "/");

        for (int i = 0; i < nuevoObjeto.Length; i++)
        {
            entidadesMundo[i] = (GameObject)Instantiate(nuevoObjeto[i]);
            string[] nombre = entidadesMundo[i].name.Split('(');
            dropBloques.options.Add(new Dropdown.OptionData() { text = nombre[0] });
            bloque[i] = nombre[0];
            Destroy(entidadesMundo[i]);
        }

        dropBloques.value = 1;
    }

    public string NombreCarpetaMundo()
    {
        if (dropMundo.value == 0) return "Ciudad";
        if (dropMundo.value == 1) return "Desierto";
        if (dropMundo.value == 2) return "jungla";
        return "Ciudad";

    }

    public void CrearBloque()
    {
        contadorBloques++;
        cuentaBloques.text = "Bloques: " + contadorBloques;
        entidad = (GameObject)Instantiate(Resources.Load("Prefabs/Niveles/" + NombreCarpetaMundo() + "/" + bloque[dropBloques.value]), Camera.main.transform.position, Quaternion.identity);
        entidad.transform.SetParent(entidadPadre.transform);
        Vector3 vector = Camera.main.transform.position;
        vector.z = 50;
        entidad.transform.position = vector;
        entidad.transform.localScale = new Vector3(1, 1, 1);
    }
}
