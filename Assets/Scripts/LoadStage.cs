﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEditor;

public class LoadStage : MonoBehaviour {


    GameObject entidad;
    Object[] nuevoObjeto;
    GameObject[] entidadesMundo = new GameObject[200];
    public GameObject entidadPadre;
    public Dropdown dropMundo, dropBloques;
    string[] bloque;

    void Start()
    {
        bloque = new string[200];
    }

    public void CrearBloque()
    {
        entidad = (GameObject)Instantiate(Resources.Load("Prefabs/Fases/" + NombreCarpetaMundo() + "/" + bloque[dropBloques.value]), Camera.main.transform.position, Quaternion.identity);
        entidad.transform.SetParent(entidadPadre.transform);
        Vector3 vector = Camera.main.transform.position;
        vector.z = 50;
        entidad.transform.position = vector;
        entidad.transform.localScale = new Vector3(1, 1, 1);
    }

    public string NombreCarpetaMundo()
    {
        if (dropMundo.value == 0) return "Ciudad";
        if (dropMundo.value == 1) return "Desierto";
        if (dropMundo.value == 2) return "jungla";
        return "Ciudad";

    }

    public void cargaEntidades()
    {
        dropBloques.ClearOptions();
        nuevoObjeto = Resources.LoadAll("Prefabs/Fases/" + NombreCarpetaMundo() + "/");

        for (int i = 0; i < nuevoObjeto.Length; i++)
        {
            entidadesMundo[i] = (GameObject)Instantiate(nuevoObjeto[i]);
            string[] nombre = entidadesMundo[i].name.Split('(');
            dropBloques.options.Add(new Dropdown.OptionData() { text = nombre[0] });
            bloque[i] = nombre[0];
            Destroy(entidadesMundo[i]);
        }

        dropBloques.value = 1;
    }

    void SaveStage()
    {
        PrefabUtility.CreatePrefab("Assets/Resources/Prefabs/Niveles/" + NombreCarpetaMundo() + "/" + bloque[dropBloques.value] + ".prefab", entidad);
    }


}
