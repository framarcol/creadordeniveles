﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEditor;

public class LoadBlockFase : MonoBehaviour {

    Object[] nuevoObjeto;
    GameObject[] entidadesMundo = new GameObject[200];
    public Dropdown dropBloques, dropMundo;
    GameObject entidad;
    string[] bloque;
    float nuevaPosicion;
    int contadorBloques;
    public GameObject entidadPadre;
    string nombre;


    public void Start()
    {
        bloque = new string[1000];
    }

    public void cargaEntidades()
    {
        dropBloques.ClearOptions();
        nuevoObjeto = Resources.LoadAll("Prefabs/Niveles/" + NombreCarpetaMundo() + "/");

        for (int i = 0; i < nuevoObjeto.Length; i++)
        {
            entidadesMundo[i] = (GameObject)Instantiate(nuevoObjeto[i]);
            string[] nombre = entidadesMundo[i].name.Split('(');
            dropBloques.options.Add(new Dropdown.OptionData() { text = nombre[0] });
            bloque[i] = nombre[0];
            Destroy(entidadesMundo[i]);
        }

        dropBloques.value = 1;
    }

    public string NombreCarpetaMundo()
    {
        if (dropMundo.value == 0) return "Ciudad";
        if (dropMundo.value == 1) return "Desierto";
        if (dropMundo.value == 2) return "jungla";
        return "Ciudad";

    }

    public void ActualizarBloque()
    {
        PrefabUtility.CreatePrefab("Assets/Resources/Prefabs/Niveles/" + NombreCarpetaMundo() + "/" + bloque[dropBloques.value] + ".prefab", entidad);
    }

    public void EditarBloque()
    {
        entidad = (GameObject)Instantiate(Resources.Load("Prefabs/Niveles/" + NombreCarpetaMundo() + "/" + bloque[dropBloques.value]), Camera.main.transform.position, Quaternion.identity);
        entidad.transform.SetParent(entidadPadre.transform);
        entidad.transform.localPosition = new Vector3(1, 8, 0);
        entidad.transform.localScale = new Vector3(1, 1, 1);
    }
}
