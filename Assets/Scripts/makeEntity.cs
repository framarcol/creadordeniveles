﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEditor;

public class makeEntity : MonoBehaviour
{

    GameObject[] elementos = new GameObject[100];
    public GameObject[] objetoNivel, entidad;
    public Dropdown dropMundo, dropTipos, dropEntidad, dropNivel;
    public Canvas canvasNiveles;
    public Dropdown nivelSeleccionado;
    public InputField nombrePrefab;
    GameObject nuevoObjeto;
    public Canvas cNivelNuevo, mainMenu;
    Vector3 posicionNivel, posicionNivelCurvas, posicionNivelEntidad;
    GameObject segundaImagen;
    public Text txtResultado;
    public GameObject objetoLinea;
    public InputField numBloquecitos;
    int contador;
    public static int iContador;
    public static float aleatorio;
    int tipoCurva;
    int i2;
    public static bool esFase;
    public static bool hayEntidad;
    public static bool esStage;
    public static bool esCrearBloque;


    void Start()
    {
        tipoCurva = 0;
        contador = 0;
        i2 = 0;
        entidad = new GameObject[100];
        mainMenu.gameObject.SetActive(true);
        cNivelNuevo.gameObject.SetActive(false);
        posicionNivel = new Vector3(1, 8, 0);
        posicionNivelCurvas = new Vector3(39, 25, 0);
        posicionNivelEntidad = new Vector3(5, -20, 0);
        
    }

    public void HacerEntidad()
    {

        entidad[i2] = (GameObject)Instantiate(Resources.Load("Prefabs/Mundos/" + NombreCarpetaMundo() + "/" + NombreCarpetaTipo() + "/" + ControladorInputs.nombres[dropEntidad.value]), posicionNivelEntidad, Quaternion.identity);
        entidad[i2].transform.SetParent(segundaImagen.transform);
        entidad[i2].transform.localScale = new Vector3(1, 1, 1);
        entidad[i2].transform.localPosition = posicionNivelEntidad;
        i2++;
        hayEntidad = true;
    }

    public void NuevoNivel(int curva)
    {
        esFase = false;
        tipoCurva = curva;
        Destroy(nuevoObjeto);
        segundaImagen = (GameObject)Instantiate(objetoNivel[curva], posicionNivel, Quaternion.identity);
        nuevoObjeto = segundaImagen;
        segundaImagen.transform.SetParent(canvasNiveles.transform);
        segundaImagen.transform.localScale = new Vector3(1, 1, 1);
        RectTransform recta = segundaImagen.GetComponent<RectTransform>();
        Vector2 vectorRecta = recta.sizeDelta;
        vectorRecta.y = int.Parse(numBloquecitos.text) * 80;
        recta.sizeDelta = vectorRecta;
        int numero = 0, numero2 = 0;
        int posPar = 0, posImpar = 0;
        float posParImpar = 40, posImparImpar = -120;
        bool espar = false;
        if (float.Parse(numBloquecitos.text) / 2 == int.Parse(numBloquecitos.text) / 2)
        {
            espar = true;
        }
        else espar = false;
        for (int i = 0; i < int.Parse(numBloquecitos.text) - 1; i++)
        {
            GameObject dibujoLinea = (GameObject)Instantiate(objetoLinea, posicionNivel, Quaternion.identity);
            dibujoLinea.transform.SetParent(segundaImagen.transform);
            if (espar)
            {
                if (numero == 0)
                {
                    dibujoLinea.transform.localPosition = new Vector3(0, 0, 0);
                    numero = 1;
                }
                else if (numero == 1)
                {
                    posImpar -= 80;
                    dibujoLinea.transform.localPosition = new Vector3(0, posImpar, 0);
                    numero = 2;
                }
                else if (numero == 2)
                {
                    posPar += 80;
                    dibujoLinea.transform.localPosition = new Vector3(0, posPar, 0);
                    numero = 1;
                }
            }
            else
            {
                if (numero2 == 0)
                {
                    dibujoLinea.transform.localPosition = new Vector3(0, -40, 0);
                    numero2 = 1;
                }
                else if (numero2 == 1)
                {
                    dibujoLinea.transform.localPosition = new Vector3(0, posParImpar, 0);
                    posParImpar += 80;
                    numero2 = 2;
                }
                else if (numero2 == 2)
                {
                    dibujoLinea.transform.localPosition = new Vector3(0, posImparImpar, 0);
                    posImparImpar -= 80;
                    numero2 = 1;
                }
            } 
            
            dibujoLinea.transform.localScale = new Vector3(1, 1, 1);
        } 
        if (curva > 0) segundaImagen.transform.localPosition = posicionNivelCurvas;
        else segundaImagen.transform.localPosition = posicionNivel;


    }

    public void Guarda()
    {
       
        if(nombrePrefab.text == "" || nombrePrefab.text == " ")
        {
            txtResultado.text = "Introduce un nombre correcto";
        }
        else
        {
            PrefabUtility.CreatePrefab("Assets/Resources/Prefabs/Niveles/" + NombreCarpetaMundoNivel() + "/" + nombrePrefab.text + ".prefab", segundaImagen);
            txtResultado.text = "Guardado con éxito";
        }

    }

    public void GuardaYNuevo()
    {
        if (nombrePrefab.text == "" || nombrePrefab.text == " ")
        {
            txtResultado.text = "Introduce un nombre correcto";
        }
        else
        {
            PrefabUtility.CreatePrefab("Assets/Resources/Prefabs/Niveles/" + NombreCarpetaMundoNivel() + "/" + nombrePrefab.text + ".prefab", segundaImagen);
            txtResultado.text = "Guardado con éxito";
            Destroy(nuevoObjeto);
            NuevoNivel(tipoCurva);
        }
    }

    public void Borrar()
    {
        Destroy(nuevoObjeto);
    }

    public void CambiarEstadoStage()
    {
        esStage = true;
    }

    public void CambiaEstadoEsFase(bool miFase)
    {
        esFase = miFase;
    }

    public void CambiaEstadoHayEntidad(bool hayenti)
    {
        hayEntidad = hayenti;
    }

    public string NombreCarpetaMundo()
    {
        if (dropMundo.value == 0) return "Ciudad";
        if (dropMundo.value == 1) return "Desierto";
        if (dropMundo.value == 2) return "jungla";
        return "Ciudad";

    }

    public string NombreCarpetaMundoNivel()
    {
        if (dropNivel.value == 0) return "Ciudad";
        if (dropNivel.value == 1) return "Desierto";
        if (dropNivel.value == 2) return "jungla";
        return "Ciudad";

    }

    public string NombreCarpetaTipo()
    {
        if (dropTipos.value == 0) return "Atrezzo";
        if (dropTipos.value == 1) return "Entidades";
        return "Atrezzo";
    }


}
  
